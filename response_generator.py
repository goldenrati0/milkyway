from all_responses import get_response


def calculate_response():
    correct_ans_response, wrong_ans_response = get_response()
    return {
        "response_to_correct_answer": correct_ans_response,
        "response_to_wrong_answer": wrong_ans_response
    }
