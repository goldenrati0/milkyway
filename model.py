from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.mysql import LONGTEXT
from datetime import datetime

import response_generator

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
db = SQLAlchemy(app=app)

"""

CREATE DATABASE `galaxy` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

^ This one did the trick. Initially I was facing problem while storing Bengali characters in MySQL Database.
Then, I found this solution in Stackoverflow.
(https://stackoverflow.com/questions/4777900/how-to-display-utf-8-characters-in-phpmyadmin)
The solution was acctually in the problen statement of this post.
While creating database I had to specify the DEFAULT CHARACTER SET to utf8 and it worked.
MySQL-cli still doesn't work. SELECT statement returns a bunch of ???????? from the table
    but any UTF-8 enabled application can read the data in Bengali.
"""


class Questions(db.Model):
    __tablename__ = 'questions'
    mysql_charset = 'utf8'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    body = db.Column('body', LONGTEXT, nullable=False)
    total_options = db.Column('total_options', db.Integer)
    added = db.Column('added', db.DateTime)
    options = db.relationship('Options', cascade='all, delete-orphan', backref='of', lazy='dynamic')

    def __init__(self, body, total_options):
        self.body = body
        self.total_options = total_options
        self.added = datetime.now()

    def json_dump(self):
        options = self.options
        options = [option.json_dump() for option in options]
        return {
            "id": self.id,
            "body": self.body,
            "total_options": self.total_options,
            "options": options,
            "response": response_generator.calculate_response()
        }

    def to_dict(self): 
        return {
            "id": self.id,
            "body": self.body
        }


class Options(db.Model):
    __tablename__ = 'options'
    mysql_charset = 'utf8'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'))
    text = db.Column('text', LONGTEXT, nullable=False)
    is_correct = db.Column('is_correct', db.Boolean)

    def __init__(self, question_id, text, is_correct=False):
        self.question_id = question_id
        self.text = text
        self.is_correct = is_correct

    def json_dump(self):
        return {
            "text": self.text,
            "is_correct": self.is_correct
        }
