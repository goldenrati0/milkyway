import random


def randomize_options_and_mark_correct_ans(question):
    all_options = question['options']
    random.shuffle(all_options)
    question['options'] = all_options
    correct_option = ''
    for i in range(len(all_options)):
        option_temp = all_options[i]
        if option_temp['is_correct']:
            if i == 0:
                correct_option = '১'
            elif i == 1:
                correct_option = '২'
            elif i == 2:
                correct_option = '৩'
            elif i == 3:
                correct_option = '৪'
            break
    question.update({'correct_option': correct_option})
    return question
