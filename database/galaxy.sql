-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 11:51 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galaxy`
--

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `text` longtext NOT NULL,
  `is_correct` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `question_id`, `text`, `is_correct`) VALUES
(1, 1, '২টি', 1),
(2, 1, '১টি', 0),
(3, 1, '৮টি', 0),
(4, 1, '৩টি', 0),
(5, 2, '-৬৬ ডিগ্রী সেলসিয়াস', 1),
(6, 2, '০ ডিগ্রী সেলসিয়াস', 0),
(7, 2, '৫০ ডিগ্রী সেলসিয়াস', 0),
(8, 2, '১০০০ ডিগ্রী সেলসিয়াস', 0),
(9, 3, 'চতুর্থ', 1),
(10, 3, 'প্রথম', 0),
(11, 3, 'দ্বিতীয়', 0),
(12, 3, 'তৃতীয়', 0),
(13, 4, 'আটাকামা মরুভুমি, চিলি', 1),
(14, 4, 'পআটাকামা মরুভুমি, অস্ট্রেলিয়া', 0),
(15, 4, 'দ্বসাহারা মরুভুমি', 0),
(16, 4, 'ইন্দাস ভ্যালি মরুভুমি, পাকিস্তান', 0),
(17, 5, 'Red Planet', 1),
(18, 5, 'Yellow Planet', 0),
(19, 5, 'Blue Planet', 0),
(20, 5, 'Brown Planet', 0),
(21, 6, 'ফোবোস', 1),
(22, 6, 'মেটিস', 0),
(23, 6, 'মুন', 0),
(24, 6, 'ইউরোপ', 0),
(25, 7, 'মঙ্গল গ্রহে জল বরফ অথবা বাষ্প হিসাবে পাওয়া যায়', 1),
(26, 7, 'মঙ্গল গ্রহে প্রচুর পরিমান তরল জল আছে', 0),
(27, 8, 'ভীষণ ঠান্ডা', 1),
(28, 8, 'ভীষণ গরম', 0),
(29, 8, 'হালকা গরম', 0),
(30, 8, 'সামান্য ঠাণ্ডা', 0),
(31, 9, 'অক্সিজেন', 1),
(32, 9, 'পানি', 0),
(33, 10, '০.১%', 1),
(34, 10, '২০%', 0),
(35, 10, '৫%', 0),
(36, 10, '৯০%', 0),
(37, 7, 'মঙ্গল গ্রহে জল শুধুমাত্র বরফ হিসাবে পাওয়া যায়', 0),
(38, 7, 'মঙ্গল গ্রহে জল শুধুমাত্র বাসবো হিসাবে পাওয়া যায়', 0),
(39, 9, 'জীবন', 0),
(40, 9, 'গাছপালা', 0);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `body` longtext NOT NULL,
  `total_options` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `body`, `total_options`, `added`) VALUES
(1, 'মঙ্গল গ্রহে কয়টি চাঁদ আছে?', 4, '2017-09-15 01:51:30'),
(2, 'মঙ্গল গ্রহে গড় তাপমাত্রা কত?', 4, '2017-09-15 01:59:53'),
(3, 'মঙ্গল গ্রহ সূর্য থেকে কততম গ্রহ?', 4, '2017-09-15 02:03:14'),
(4, 'আমাদের পৃথিবীতে মঙ্গল গ্রহের মতো একটি জায়গা রয়েছে.জায়গাটির নাম কি এবং জায়গাটি কোথায় অবস্থিত?', 4, '2017-09-15 02:05:37'),
(5, 'মঙ্গল গ্রহকে বলা হয় -', 4, '2017-09-15 02:09:04'),
(6, 'নিচের কোনটি মঙ্গল গ্রহের চাঁদ?', 4, '2017-09-15 02:10:18'),
(7, 'নিচের কোনটি সঠিক?', 4, '2017-09-15 02:18:25'),
(8, 'মঙ্গোল গ্রহের বায়ুমণ্ডল -', 4, '2017-09-15 02:20:07'),
(9, 'বিজ্ঞানীরা  বিশ্বাস করেন যে এক সময়  মঙ্গলে  অনেক _____________ ছিল ।', 4, '2017-09-15 02:21:32'),
(10, 'মঙ্গল গ্রহে কত শতাংশ অক্সিজেন রয়েছে?', 4, '2017-09-15 02:22:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
